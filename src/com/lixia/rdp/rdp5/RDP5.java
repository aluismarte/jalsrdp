/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lixia.rdp.rdp5;

import com.lixia.rdp.OrderException;
import com.lixia.rdp.RdesktopException;
import com.lixia.rdp.crypto.CryptoException;
import com.lixia.rdp.Package.RdpPackage;
import java.io.IOException;

/**
 * @author aluis
 */
public class RDP5 extends ARDP5 {

    private final VChannels channels;

    /**
     * Initialise the RDP5 communications layer, with specified virtual channels
     *
     * @param channels Virtual channels for RDP layer
     */
    public RDP5(VChannels channels) {
        super(channels);
        this.channels = channels;
    }

    /**
     * Process an RDP5 packet
     *
     * @param s Packet to be processed
     * @param e True if packet is encrypted
     * @throws RdesktopException
     * @throws OrderException
     * @throws CryptoException
     */
    public void rdp5_process(RdpPackage s, boolean e) throws RdesktopException, OrderException, CryptoException {
        rdp5_process(s, e, false);
    }

    /**
     * Process an RDP5 packet
     *
     * @param s Packet to be processed
     * @param encryption True if packet is encrypted
     * @param shortform True if packet is of the "short" form
     * @throws RdesktopException
     * @throws OrderException
     * @throws CryptoException
     */
    public void rdp5_process(RdpPackage s, boolean encryption, boolean shortform) throws RdesktopException, OrderException, CryptoException {
        int length, count;
        int type;
        int next;

        if (encryption) {
            s.incrementPosition(8);/* signature */

            byte[] data = new byte[s.size() - s.getPosition()];
            s.copyToByteArray(data, 0, s.getPosition(), data.length);
            byte[] packet = SecureLayer.decrypt(data);
            s.copyFromByteArray(packet, 0, s.getPosition(), packet.length);
        }

        while (s.getPosition() < s.getEnd()) {
            type = s.get8();
            if ((type & 0x80) > 0) {
                type ^= 0x80;
            } else {
            }
            length = s.getLittleEndian16();
            /* next_packet = */
            next = s.getPosition() + length;
//            logger.debug("RDP5: type = " + type);
            switch (type) {
                case 0: /* orders */

                    count = s.getLittleEndian16();
                    orders.processOrders(s, next, count);
                    break;
                case 1: /* bitmap update (???) */

                    s.incrementPosition(2); /* part length */

                    processBitmapUpdates(s);
                    break;
                case 2: /* palette */

                    s.incrementPosition(2);
                    processPalette(s);
                    break;
                case 3: /* probably an palette with offset 3. Weird */

                    break;
                case 5:
                    process_null_system_pointer_pdu(s);
                    break;
                case 6: // default pointer
                    break;
                case 9:
                    process_colour_pointer_pdu(s);
                    break;
                case 10:
                    process_cached_pointer_pdu(s);
                    break;
                default:
//                    logger.warn("Unimplemented RDP5 opcode " + type);
            }
            s.setPosition(next);
        }
    }

    /**
     * Process an RDP5 packet from a virtual channel
     *
     * @param s Packet to be processed
     * @param channelno Channel on which packet was received
     */
    void rdp5_process_channel(RdpPackage s, int channelno) {
        VChannel channel = channels.find_channel_by_channelno(channelno);
        if (channel != null) {
            try {
                channel.process(s);
            } catch (RdesktopException ex) {
            } catch (IOException ex) {
            } catch (CryptoException ex) {
            }
        }
    }
}
