package com.alssoftrd;

import com.alssoftrd.events.EventWakeUp;
import com.alssoftrd.events.KeyBoard;
import com.alssoftrd.events.Mouse;
import com.alssoftrd.utils.Configuration;
import com.alssoftrd.utils.RDPConnection;
import com.alssoftrd.utils.ShadowCanvas;
import java.awt.Image;
import java.util.ArrayList;

/**
 * @author aluis
 */
public abstract class JAlsRdp extends RDPConnection {

    // Need disconect exception to tell user.
    // Provide a reconnect secuence.
    private final EventWakeUp ewu;
    private final Mouse mouse;
    private final KeyBoard keyboard;

    public JAlsRdp(Configuration conf) {
        super(conf);
        ewu = new EventWakeUp(this);
        mouse = new Mouse();
        keyboard = new KeyBoard();
    }

    public void connectRDP() {
        this.start();
    }

    /**
     * Manage send inputs. Use the constants in Mouse class.
     *
     * @param state a constant to define what mouse do.
     * @param button button mouse pressed (when move send 0).
     * @param x coord in x of mouse.
     * @param y coord in y of mouse.
     */
    public void sendMouseEvent(int state, int button, int x, int y) {
        switch (state) {
            case Mouse.stateMouseMove:
                mouse.mouseMoved(x, y);
                break;
            case Mouse.stateMousePressed:
                mouse.mousePressed(button, x, y);
                break;
            case Mouse.stateMouseRelease:
                mouse.mouseReleased(button, x, y);
                break;
        }
    }

    /**
     * @param key key press
     * @param typeEvent 0 press, 1 release, 2 quiet
     */
    public void sendKeyEvent(int key, int typeEvent) {
        ArrayList<Integer> special = new ArrayList<Integer>();
        switch (typeEvent) {
            case 0:
                keyboard.keyPressed(key, special);
                break;
            case 1:
                keyboard.keyReleased(key, special);
                break;
            case 2:
                keyboard.keyTyped(key, special);
                break;
        }
    }

    /**
     * When you use more than 1 key pressed. Only especial keys. Example: CTRL,
     * ALT, SHIFT
     *
     * @param key Send key pressed in one event.
     * @param specials Send CTRL and other similar.
     * @param typeEvent 0 press, 1 release, 2 quiet.
     */
    public void sendKeyEvent(int key, int[] specials, int typeEvent) {
        ArrayList<Integer> sp = new ArrayList<Integer>();
        for (int i = 0; i < specials.length; i++) {
            sp.add(specials[i]);
        }
        switch (typeEvent) {
            case 0:
                keyboard.keyPressed(key, sp);
                break;
            case 1:
                keyboard.keyReleased(key, sp);
                break;
            case 2:
                keyboard.keyTyped(key, sp);
                break;
        }
    }

    // Manage recibe inputs;
    /**
     * Complete image replace. Base for PC or mobile apps. Not recommended for
     * html5 client
     *
     * @param img Image complete
     */
    public abstract void image(Image img);

    /**
     * Only image part change. Recommended for html5 client.
     *
     * @param x Position of axis x
     * @param y Position of axis y
     * @param img Segment image (only change part);
     */
    public void imageSegmented(int x, int y, Image img) {
//        File file = new File("" + a + ".png");
//        System.out.println("X: " + posX + " Y: " + posY);
//        BufferedImage im = (BufferedImage) img;
//        try {
//            ImageIO.write(im, "png", file);
//        } catch (IOException e) {
//        }
//        a++;
    }

    public String getImageURL() {
        return sc.getImageToUrl();
    }

    public String getImageURL(Image img) {
        return sc.getImageURL(img);
    }

    // Manage Wake Up Events
    public void setImage(ShadowCanvas sc) {
        image(sc.getImage());
    }

}
