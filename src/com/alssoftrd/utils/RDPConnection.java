package com.alssoftrd.utils;

import com.lixia.rdp.ConnectionException;
import com.lixia.rdp.OrderException;
import com.lixia.rdp.RdesktopException;
import com.lixia.rdp.crypto.CryptoException;
import com.lixia.rdp.rdp5.RDP5;
import com.lixia.rdp.rdp5.VChannels;
import com.lixia.rdp.rdp5.keys.KeysChannel;
import com.lixia.rdp.rdp5.rdpdr.Rdpdr;
import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;

/**
 * @author aluis
 */
public abstract class RDPConnection extends Thread {

    /**
     * Translate a disconnect code into a textual description of the reason for
     * the disconnect
     *
     * @param reason Integer disconnect code received from server
     * @return Text description of the reason for disconnection
     */
    private String textDisconnectReason(int reason) {
        String text;

        switch (reason) {
            case exDiscReasonNoInfo:
                text = "No information available";
                break;

            case exDiscReasonAPIInitiatedDisconnect:
                text = "Server initiated disconnect";
                break;

            case exDiscReasonAPIInitiatedLogoff:
                text = "Server initiated logoff";
                break;

            case exDiscReasonServerIdleTimeout:
                text = "Server idle timeout reached";
                break;

            case exDiscReasonServerLogonTimeout:
                text = "Server logon timeout reached";
                break;

            case exDiscReasonReplacedByOtherConnection:
                text = "Another user connected to the session";
                break;

            case exDiscReasonOutOfMemory:
                text = "The server is out of memory";
                break;

            case exDiscReasonServerDeniedConnection:
                text = "The server denied the connection";
                break;

            case exDiscReasonServerDeniedConnectionFips:
                text = "The server denied the connection for security reason";
                break;

            case exDiscReasonLicenseInternal:
                text = "Internal licensing error";
                break;

            case exDiscReasonLicenseNoLicenseServer:
                text = "No license server available";
                break;

            case exDiscReasonLicenseNoLicense:
                text = "No valid license available";
                break;

            case exDiscReasonLicenseErrClientMsg:
                text = "Invalid licensing message";
                break;

            case exDiscReasonLicenseHwidDoesntMatchLicense:
                text = "Hardware id doesn't match software license";
                break;

            case exDiscReasonLicenseErrClientLicense:
                text = "Client license error";
                break;

            case exDiscReasonLicenseCantFinishProtocol:
                text = "Network error during licensing protocol";
                break;

            case exDiscReasonLicenseClientEndedProtocol:
                text = "Licensing protocol was not completed";
                break;

            case exDiscReasonLicenseErrClientEncryption:
                text = "Incorrect client license enryption";
                break;

            case exDiscReasonLicenseCantUpgradeLicense:
                text = "Can't upgrade license";
                break;

            case exDiscReasonLicenseNoRemoteConnections:
                text = "The server is not licensed to accept remote connections";
                break;

            default:
                if (reason > 0x1000 && reason < 0x7fff) {
                    text = "Internal protocol error";
                } else {
                    text = "Unknown reason";
                }
        }
        return text;
    }

    /* RDP5 disconnect PDU */
    private final int exDiscReasonNoInfo = 0x0000;
    private final int exDiscReasonAPIInitiatedDisconnect = 0x0001;
    private final int exDiscReasonAPIInitiatedLogoff = 0x0002;
    private final int exDiscReasonServerIdleTimeout = 0x0003;
    private final int exDiscReasonServerLogonTimeout = 0x0004;
    private final int exDiscReasonReplacedByOtherConnection = 0x0005;
    private final int exDiscReasonOutOfMemory = 0x0006;
    private final int exDiscReasonServerDeniedConnection = 0x0007;
    private final int exDiscReasonServerDeniedConnectionFips = 0x0008;
    private final int exDiscReasonLicenseInternal = 0x0100;
    private final int exDiscReasonLicenseNoLicenseServer = 0x0101;
    private final int exDiscReasonLicenseNoLicense = 0x0102;
    private final int exDiscReasonLicenseErrClientMsg = 0x0103;
    private final int exDiscReasonLicenseHwidDoesntMatchLicense = 0x0104;
    private final int exDiscReasonLicenseErrClientLicense = 0x0105;
    private final int exDiscReasonLicenseCantFinishProtocol = 0x0106;
    private final int exDiscReasonLicenseClientEndedProtocol = 0x0107;
    private final int exDiscReasonLicenseErrClientEncryption = 0x0108;
    private final int exDiscReasonLicenseCantUpgradeLicense = 0x0109;
    private final int exDiscReasonLicenseNoRemoteConnections = 0x010a;

//    static Logger logger = Logger.getLogger("com.lixia.rdp");

    private static boolean keep_running;

    private KeysChannel keyChannel;
    private final VChannels channels;

    public static RDP5 RdpLayer = null;

    // Mouse control
    private int x;
    private int y;

    // Image manage
    public static ShadowCanvas sc;

    // Configuration
    public static Configuration conf;

    public RDPConnection(Configuration conf) {
//        BasicConfigurator.configure();
//        logger.setLevel(org.apache.log4j.Level.DEBUG);

        sc = new ShadowCanvas();

        RDPConnection.conf = conf;

        keep_running = true;
        channels = new VChannels();

        // Initialise all RDP5 channels
        if (conf.use_rdp5) {
            Rdpdr rdpdrChannel = new Rdpdr();
            try {
                channels.register(rdpdrChannel);
            } catch (RdesktopException ex) {
                java.util.logging.Logger.getLogger(RDPConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        String os = System.getProperty("os.name");

        if (os.equals("Windows 2000") || os.equals("Windows XP")) {
            conf.built_in_licence = true;
        }

        if (os.startsWith("Linux")) {
            Configuration.OS = conf.LINUX;
        } else if (os.startsWith("Windows")) {
            Configuration.OS = conf.WINDOWS;
        } else if (os.startsWith("Mac")) {
            Configuration.OS = conf.MAC;
        }

        if (Configuration.OS == conf.MAC) {
            conf.caps_sends_up_and_down = false;
        }

        conf.rdp = RdpLayer;
    }

    @Override
    public void run() {
        boolean[] deactivated = new boolean[1];
        int[] ext_disc_reason = new int[1];

        while (keep_running) {
            RdpLayer = new RDP5(channels);
            conf.rdp = RdpLayer;

            conf.loggedon = false;
            conf.readytosend = false;

            if (RdpLayer != null) {
                try {
                    RdpLayer.connect(conf.username, InetAddress.getByName(conf.server), conf.domain, conf.password, conf.command, conf.directory);
                    if (keep_running) {
                        if (!conf.packet_encryption) {
                            conf.encryption = false;
                        }
                        try {
                            RdpLayer.mainLoop(deactivated, ext_disc_reason);
                        } catch (RdesktopException ex) {
                            java.util.logging.Logger.getLogger(RDPConnection.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (OrderException ex) {
                            java.util.logging.Logger.getLogger(RDPConnection.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (CryptoException ex) {
                            java.util.logging.Logger.getLogger(RDPConnection.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (deactivated[0]) {
                            disconnectRDP();
                        } else {
                            if (ext_disc_reason[0] == exDiscReasonAPIInitiatedDisconnect || ext_disc_reason[0] == exDiscReasonAPIInitiatedLogoff) {
                                disconnectRDP();
                            }
                            if (ext_disc_reason[0] >= 2) {
                                String reason = textDisconnectReason(ext_disc_reason[0]);
                                disconnectRDP();
                            }
                        }
                        keep_running = false;
                        if (!conf.readytosend) {
                            String msg1 = "The terminal server disconnected before licence negotiation completed.";
                            String msg2 = "Possible cause: terminal server could not issue a licence.";
                            disconnectRDP();
                        }
                    }
                } catch (IOException ex) {
                    String msg = "IO Exception";

                } catch (ConnectionException ex) {
                    String msg = "Server can not be reached!. Please try again later.";

                }
            }
        }
        disconnectRDP();
    }

    public static void off() {
        keep_running = false;
        if (RdpLayer != null && RdpLayer.isConnected()) {
            RdpLayer.disconnect();
        }
    }

    public void disconnectRDP() {
        keep_running = false;
        if (RdpLayer != null && RdpLayer.isConnected()) {
            RdpLayer.disconnect();
        }
    }

}
