package com.alssoftrd.utils;

import com.lixia.rdp.MCS;
import com.lixia.rdp.Secure;
import com.lixia.rdp.rdp5.RDP5;
import java.awt.image.DirectColorModel;

/**
 * @author aluis
 */
public class Configuration {

    public String server = "127.0.0.1";

    public boolean loggedon = false;
    public boolean readytosend = false;

    public final int DIRECT_BITMAP_DECOMPRESSION = 0;
    public final int BUFFEREDIMAGE_BITMAP_DECOMPRESSION = 1;
    public final int INTEGER_BITMAP_DECOMPRESSION = 2;

    public int bitmap_decompression_store = INTEGER_BITMAP_DECOMPRESSION;

    public boolean low_latency = true; // disables bandwidth saving tcp packets
    public int keylayout = 0x409; // EN-US by default
    public String username = "ROOT"; // -u username
    public String domain = ""; // -d domain
    public String password = ""; // -p password
    public String hostname = ""; // -n hostname
    public String command = "";  // -s command
    public String directory = ""; // -d directory
    public String windowTitle = "Elusiva Everywhere"; // -T windowTitle
    public int width = 800; // -g widthxheight
    public int height = 600; // -g widthxheight
    public int port = 3389; // -t port
    public boolean fullscreen = false;
    public boolean built_in_licence = false;

    //setting
    public boolean autologin = false;
    public boolean bulk_compression = false;
    public boolean console_audio = false;

    public boolean load_licence = false;
    public boolean save_licence = false;

    public String licence_path = "./";

    public boolean debug_keyboard = false;
    public boolean debug_hexdump = false;

    public boolean enable_menu = false;
    //public boolean paste_hack = true;
    public boolean no_loginProgress = false;

    public boolean http_mode = false;
    public String http_server = "192.168.0.115:8080/WSService/RDPSocket";

    public int screenInsets_title = 14;

    public boolean altkey_quiet = false;
    public boolean caps_sends_up_and_down = true;
    public boolean remap_hash = true;
    public boolean useLockingKeyState = true;

    public boolean use_rdp5 = true;
    public int server_bpp = 24;							// Bits per pixel
    public int Bpp = (server_bpp + 7) / 8;				// Bytes per pixel

    public int bpp_mask = 0xFFFFFF >> 8 * (3 - Bpp);	// Correction value to ensure only the relevant
    // number of bytes are used for a pixel

    public int imgCount = 0;

    public DirectColorModel colour_model = new DirectColorModel(24, 0xFF0000, 0x00FF00, 0x0000FF);

    /**
     * Set a new value for the server's bits per pixel
     *
     * @param server_bpp New bpp value
     */
    public void set_bpp(int server_bpp) {
        this.server_bpp = server_bpp;
        Bpp = (server_bpp + 7) / 8;
        bpp_mask = 0xFFFFFF >> 8 * (3 - Bpp);
        colour_model = new DirectColorModel(24, 0xFF0000, 0x00FF00, 0x0000FF);
    }

    public int server_rdp_version;

    public int win_button_size = 0;	/* If zero, disable single app mode */

    public boolean bitmap_compression = true;
    public boolean persistent_bitmap_caching = false;
    public boolean bitmap_caching = true;
    public boolean precache_bitmaps = true;
    public boolean polygon_ellipse_orders = false;
    public boolean sendmotion = true;
    public boolean orders = true;
    public boolean packet_encryption = true;
    public boolean grab_keyboard = true;
    public boolean hide_decorations = false;
    public boolean console_session = false;
    public boolean owncolmap;

    public boolean use_ssl = false;
    public boolean map_clipboard = true;
    public int rdp5_performanceflags = RDP5.RDP5_NO_CURSOR_SHADOW | RDP5.RDP5_NO_CURSORSETTINGS
            | RDP5.RDP5_NO_FULLWINDOWDRAG | RDP5.RDP5_NO_MENUANIMATIONS
            | RDP5.RDP5_NO_THEMING | RDP5.RDP5_NO_WALLPAPER;
    public boolean save_graphics = false;

    public boolean keys_register = true;
    public boolean is_debug = false;

    // Constant
    public final boolean desktop_save = true;

    public final boolean SystemExit = true;
    public boolean encryption = true;
    public boolean licence = true;

    public final int WINDOWS = 1;
    public final int LINUX = 2;
    public final int MAC = 3;

    public static int OS = 0;

    // Common
    public boolean underApplet = false;
    public RDP5 rdp;
    public Secure secure;
    public MCS mcs;

    
    /**
     * Constructor.
     * Configure others parameters directly
     * 
     * @param server
     * @param username
     * @param password
     * @param low_Latency
    */
    public Configuration(String server, String username, String password, boolean low_Latency) {
        this.server = server;
        this.username = username;
        this.password = password;
        this.low_latency = low_Latency;
    }
    
    /**
     * Constructor.
     * Configure others parameters directly
     * 
     * @param server
     * @param port
     * @param username
     * @param password
     * @param low_Latency
    */
    public Configuration(String server, int port, String username, String password, boolean low_Latency) {
        this.server = server;
        this.username = username;
        this.password = password;
        this.low_latency = low_Latency;
        this.port = port;
    }
    
    private void off() {
        RDPConnection.off();
    }
    
    public void exit() {
        off();
    }
}
