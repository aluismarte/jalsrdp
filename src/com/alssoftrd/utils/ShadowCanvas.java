package com.alssoftrd.utils;

import com.lixia.rdp.WrappedImage;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

/**
 * @author aluis
 */
public class ShadowCanvas {

    private BufferedImage img;

    public ShadowCanvas() {
    }

    public ShadowCanvas(WrappedImage wi) {
        img = wi.getBufferedImage();
    }

    public void setImage(WrappedImage wi) {
        img = wi.getBufferedImage();
    }

    public void setImage(Image img) {
        this.img = (BufferedImage) img;
    }

    public Image getImage() {
        return (Image) img;
    }

    public String getImageToUrl() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, "png", baos);
        } catch (IOException ex) {
            Logger.getLogger(ShadowCanvas.class.getName()).log(Level.SEVERE, null, ex);
        }
        //byte imageData[] = baos.toByteArray();
        //return "data:image/png;base64," + Base64.encodeBase64URLSafeString(imageData);
        return "data:image/png;base64," + DatatypeConverter.printBase64Binary(baos.toByteArray());
    }

    public String getImageURL(Image imge) {
        BufferedImage image = (BufferedImage) imge;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "png", baos);
        } catch (IOException ex) {
            Logger.getLogger(ShadowCanvas.class.getName()).log(Level.SEVERE, null, ex);
        }
        //byte imageData[] = baos.toByteArray();
        //return "data:image/png;base64," + Base64.encodeBase64URLSafeString(imageData);
        return "data:image/png;base64," + DatatypeConverter.printBase64Binary(baos.toByteArray());
    }

}
