package com.alssoftrd.events;

import com.alssoftrd.JAlsRdp;

/**
 * @author aluis
 */
public class EventWakeUp {

    private static JAlsRdp rdp;
    
    public EventWakeUp(JAlsRdp rdp) {
        EventWakeUp.rdp = rdp;
    }
    
    public static JAlsRdp getInstance() {
        return rdp;
    }
    
}