package com.alssoftrd.events;

import com.alssoftrd.utils.RDPConnection;
import java.util.ArrayList;

/**
 * @author aluis
 */
public class KeyBoard {

    private static final int KBD_FLAG_QUIET = 0x200;
    private static final int KBD_FLAG_DOWN = 0x4000;
    private static final int KBD_FLAG_UP = 0x8000;

    private static final int RDP_KEYPRESS = 0;
    private static final int RDP_KEYRELEASE = KBD_FLAG_DOWN | KBD_FLAG_UP;

    private static final int RDP_INPUT_SCANCODE = 4;

    private static int time = 0;

    public static final int shift = 0x2a;
    public static final int leftALT = 0x38;
    public static final int leftCTRL = 0x1d;
//    public final int del = 0x53;
//    public final int escape = 0x01;
//    public final int tab = 0x0f;

    /**
     * Retrieve the next "timestamp", by incrementing previous stamp (up to the
     * maximum value of an integer, at which the timestamp is reverted to 1)
     *
     * @return New timestamp value
     */
    public static int getTime() {
        time++;
        if (time == Integer.MAX_VALUE) {
            time = 1;
        }
        return time;
    }

    private void sendKey(int flags, int scancode) {
        RDPConnection.RdpLayer.sendInput((int) time, RDP_INPUT_SCANCODE, flags, scancode, 0);
    }

    /**
     * @param scancode key press
     * @param specialScancode CTRL, SHIFT and ALT
     */
    public void keyPressed(int scancode, ArrayList<Integer> specialScancode) {
        if (RDPConnection.RdpLayer != null) {
            sendKey(RDP_KEYPRESS, scancode);
            for (Integer entero : specialScancode) {
                sendKey(RDP_KEYPRESS, entero);
            }
        }
    }

    /**
     * @param scancode key press
     * @param specialScancode CTRL, SHIFT and ALT
     */
    public void keyTyped(int scancode, ArrayList<Integer> specialScancode) {
        if (RDPConnection.RdpLayer != null) {
            sendKey(RDP_KEYPRESS, scancode);
            for (Integer entero : specialScancode) {
                sendKey(RDP_KEYPRESS, entero);
            }
            sendKey(RDP_KEYRELEASE, scancode);
            for (Integer entero : specialScancode) {
                sendKey(RDP_KEYRELEASE, entero);
            }
        }
    }

    /**
     * @param scancode key press
     * @param specialScancode CTRL, SHIFT and ALT
     */
    public void keyReleased(int scancode, ArrayList<Integer> specialScancode) {
        if (RDPConnection.RdpLayer != null) {
            sendKey(RDP_KEYRELEASE, scancode);
            for (Integer entero : specialScancode) {
                sendKey(RDP_KEYRELEASE, entero);
            }
        }
    }

}
