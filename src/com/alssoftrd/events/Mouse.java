package com.alssoftrd.events;

import com.alssoftrd.utils.RDPConnection;

/**
 * @author aluis
 */
public class Mouse {

    private static int time = 0;

    private static final int MOUSE_FLAG_MOVE = 0x0800;

    private static final int MOUSE_FLAG_BUTTON1 = 0x1000; // Left
    private static final int MOUSE_FLAG_BUTTON2 = 0x2000; // Right
    private static final int MOUSE_FLAG_BUTTON3 = 0x4000; // Middle

    private static final int MOUSE_FLAG_BUTTON4 = 0x0280; // wheel up - rdesktop 1.2.0
    private static final int MOUSE_FLAG_BUTTON5 = 0x0380; // wheel down - rdesktop 1.2.0

    private static final int MOUSE_FLAG_DOWN = 0x8000;
    private static final int RDP_INPUT_MOUSE = 0x8001;

    // Constants
    public static final int stateMouseMove = 0;
    public static final int stateMousePressed = 1;
    public static final int stateMouseRelease = 2;

    public static final int mouseLeftClick = 16;
    public static final int mouseRightClick = 4;
    public static final int mouseMiddleClick = 8;

    /**
     * Retrieve the next "timestamp", by incrementing previous stamp (up to the
     * maximum value of an integer, at which the timestamp is reverted to 1)
     *
     * @return New timestamp value
     */
    public static int getTime() {
        time++;
        if (time == Integer.MAX_VALUE) {
            time = 1;
        }
        return time;
    }

    public void mousePressed(int button, int x, int y) {
        int tm = getTime();
        if (RDPConnection.RdpLayer != null) {
            if (button == mouseLeftClick) {
                RDPConnection.RdpLayer.sendInput(tm, RDP_INPUT_MOUSE, MOUSE_FLAG_BUTTON1 | MOUSE_FLAG_DOWN, x, y);
            } else if (button == mouseRightClick) {
                RDPConnection.RdpLayer.sendInput(tm, RDP_INPUT_MOUSE, MOUSE_FLAG_BUTTON2 | MOUSE_FLAG_DOWN, x, y);
            } else if (button == mouseMiddleClick) {
                middleButtonPressed(x, y);
            }
        }
    }

    public void mouseReleased(int button, int x, int y) {
        int tm = getTime();
        if (RDPConnection.RdpLayer != null) {
            if (button == mouseLeftClick) {
                RDPConnection.RdpLayer.sendInput(tm, RDP_INPUT_MOUSE, MOUSE_FLAG_BUTTON1, x, y);
            } else if (button == mouseRightClick) {
                RDPConnection.RdpLayer.sendInput(tm, RDP_INPUT_MOUSE, MOUSE_FLAG_BUTTON2, x, y);
            } else if (button == mouseMiddleClick) {
                middleButtonReleased(x, y);
            }
        }
    }

    private void middleButtonPressed(int x, int y) {
        RDPConnection.RdpLayer.sendInput(time, RDP_INPUT_MOUSE, MOUSE_FLAG_BUTTON3 | MOUSE_FLAG_DOWN, x, y);
    }

    private void middleButtonReleased(int x, int y) {
        RDPConnection.RdpLayer.sendInput(time, RDP_INPUT_MOUSE, MOUSE_FLAG_BUTTON3, x, y);
    }

    public void mouseMoved(int x, int y) {
        int tm = getTime();
        if (RDPConnection.RdpLayer != null) {
            RDPConnection.RdpLayer.sendInput(tm, RDP_INPUT_MOUSE, MOUSE_FLAG_MOVE, x, y);
        }
    }

    // it supposed to use
    public void mouseDragged(int x, int y) {
        int tm = getTime();
        if (RDPConnection.RdpLayer != null) {
            RDPConnection.RdpLayer.sendInput(tm, RDP_INPUT_MOUSE, MOUSE_FLAG_MOVE, x, y);
        }
    }

}
